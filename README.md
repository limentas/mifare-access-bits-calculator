# Mifare access bits calculator

This project is demo of Qt Webassembly module. Mifare access bits calculator allows you to calculate access bits by functional description and vice versa parse access bits.
You can check the result at https://limentas.bitbucket.io/mifarecalc/
