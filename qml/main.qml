import QtQuick 2.13
import QtQuick.Controls 2.12
import QtQuick.Window 2.13
import QtQuick.Layouts 1.3

Window {
    visible: true
    width: 760
    height: 480
    title: qsTr("Mifare access bits calculator")

    RowLayout {
        anchors.fill: parent

        Item {
            Layout.fillWidth: true
            Layout.preferredWidth: 1
            height: 10
        }

        ColumnLayout {
            Layout.fillHeight: true
            Layout.fillWidth: true
            Layout.topMargin: 5
            spacing: 10

            Rectangle {
                id: errorMessage
                Layout.alignment: Qt.AlignHCenter
                height: 40
                width: 500
                border.width: 1
                color: "#ffeeee"
                visible: false

                Text {
                    id: errorText
                    anchors.centerIn: parent
                    text: ""
                }
            }

            Item { // Error message placeholder
                height: errorMessage.height
                width: 10
                visible: !errorMessage.visible
            }

            TextField {
                id: accessBitsText
                Layout.alignment: Qt.AlignHCenter
                text: "FF0780"
                inputMask: "HHHHHHhh"
                horizontalAlignment: TextInput.AlignHCenter
                verticalAlignment: TextInput.AlignVCenter
                onTextChanged: {
                    if (accessBitsText.acceptableInput)
                    {
                        errorMessage.visible = false
                    }
                    else
                    {
                        errorText.text = "Not enouth data for decode access bits"
                        errorMessage.visible = true
                    }
                }
            }

            RowLayout {
                Layout.alignment: Qt.AlignHCenter
                Button {
                    enabled: accessBitsText.acceptableInput
                    text: "Decode"
                    icon.source: "qrc:/img/keyboard_arrow_down-24px.svg"
                    icon.height: height / 2
                    icon.width: height / 2
                    onClicked: decodeString(accessBitsText.text)
                }
                Item { //placeholder between buttons
                    width: 10
                }
                Button {
                    text: "Encode"
                    icon.source: "qrc:/img/keyboard_arrow_up-24px.svg"
                    icon.height: height / 2
                    icon.width: height / 2
                    onClicked: encode()
                }
            }

            ColumnLayout {
                Layout.fillHeight: true
                Layout.alignment: Qt.AlignHCenter
                spacing: 20

                BlocksConditions {
                    id: blocksConditions
                    Layout.alignment: Qt.AlignHCenter
                    Layout.fillWidth: true
                }

                SectorTrailerConditions {
                    id: sectorTrailerConditions
                    Layout.alignment: Qt.AlignHCenter
                    Layout.fillWidth: true
                    Layout.preferredWidth: blocksConditions.contentWidth + 30
                    Layout.maximumWidth: blocksConditions.contentWidth + 30
                }

                Item {
                    Layout.fillHeight: true
                    height: 1
                }
            }
        }

        Item {
            Layout.fillWidth: true
            Layout.preferredWidth: 1
            height: 10
        }
    }

    function decodeString(accessBitsStr)
    {
        if (accessBitsStr.length > 6)
            accessBitsStr = accessBitsStr.substr(0, 6)
        decode(parseInt(accessBitsStr, 16))
    }

    function decode(accessBits)
    {
        var c1_0 = (accessBits & 0x1000) != 0;
        if (c1_0 === ((accessBits & 0x10000) != 0)) //in this postition c1_0 should be inversed
        {
            showParseAccessBitsError()
            return;
        }

        var c2_0 = (accessBits & 0x01) != 0;
        if (c2_0 === ((accessBits & 0x100000) != 0))
        {
            showParseAccessBitsError()
            return;
        }

        var c3_0 = (accessBits & 0x10) != 0;
        if (c3_0 === ((accessBits & 0x100) != 0))
        {
            showParseAccessBitsError()
            return;
        }

        var c1_1 = (accessBits & 0x2000) != 0;
        if (c1_1 === ((accessBits & 0x20000) != 0))
        {
            showParseAccessBitsError()
            return;
        }

        var c2_1 = (accessBits & 0x02) != 0;
        if (c2_1 === ((accessBits & 0x200000) != 0))
        {
            showParseAccessBitsError()
            return;
        }

        var c3_1 = (accessBits & 0x20) != 0;
        if (c3_1 === ((accessBits & 0x200) != 0))
        {
            showParseAccessBitsError()
            return;
        }

        var c1_2 = (accessBits & 0x4000) != 0;
        if (c1_2 === ((accessBits & 0x40000) != 0))
        {
            showParseAccessBitsError()
            return;
        }

        var c2_2 = (accessBits & 0x04) != 0;
        if (c2_2 === ((accessBits & 0x400000) != 0))
        {
            showParseAccessBitsError()
            return;
        }

        var c3_2 = (accessBits & 0x40) != 0;
        if (c3_2 === ((accessBits & 0x400) != 0))
        {
            showParseAccessBitsError()
            return;
        }

        var c1_3 = (accessBits & 0x8000) != 0;
        if (c1_3 === ((accessBits & 0x80000) != 0))
        {
            showParseAccessBitsError()
            return;
        }

        var c2_3 = (accessBits & 0x08) != 0;
        if (c2_3 === ((accessBits & 0x800000) != 0))
        {
            showParseAccessBitsError()
            return;
        }

        var c3_3 = (accessBits & 0x80) != 0;
        if (c3_3 === ((accessBits & 0x800) != 0))
        {
            showParseAccessBitsError()
            return;
        }

        blocksConditions.c1_0 = c1_0
        blocksConditions.c2_0 = c2_0
        blocksConditions.c3_0 = c3_0

        blocksConditions.c1_1 = c1_1
        blocksConditions.c2_1 = c2_1
        blocksConditions.c3_1 = c3_1

        blocksConditions.c1_2 = c1_2
        blocksConditions.c2_2 = c2_2
        blocksConditions.c3_2 = c3_2

        sectorTrailerConditions.c1 = c1_3
        sectorTrailerConditions.c2 = c2_3
        sectorTrailerConditions.c3 = c3_3

        blocksConditions.decode()
        sectorTrailerConditions.decode()
    }

    function encode()
    {
        var accessBits = 0;
        accessBits |= blocksConditions.c2_0;
        accessBits |= blocksConditions.c2_1 ? 0x02 : 0x00;
        accessBits |= blocksConditions.c2_2 ? 0x04 : 0x00;
        accessBits |= sectorTrailerConditions.c2 ? 0x08 : 0x00;

        accessBits |= blocksConditions.c3_0 ? 0x10 : 0x00;
        accessBits |= blocksConditions.c3_1 ? 0x20 : 0x00;
        accessBits |= blocksConditions.c3_2 ? 0x40 : 0x00;
        accessBits |= sectorTrailerConditions.c3 ? 0x80 : 0x00;

        accessBits |= ! blocksConditions.c3_0 ? 0x100 : 0x00;
        accessBits |= ! blocksConditions.c3_1 ? 0x200 : 0x00;
        accessBits |= ! blocksConditions.c3_2 ? 0x400 : 0x00;
        accessBits |= ! sectorTrailerConditions.c3 ? 0x800 : 0x00;

        accessBits |= blocksConditions.c1_0 ? 0x1000 : 0x00;
        accessBits |= blocksConditions.c1_1 ? 0x2000 : 0x00;
        accessBits |= blocksConditions.c1_2 ? 0x4000 : 0x00;
        accessBits |= sectorTrailerConditions.c1 ? 0x8000 : 0x00;

        accessBits |= ! blocksConditions.c1_0 ? 0x10000 : 0x00;
        accessBits |= ! blocksConditions.c1_1 ? 0x20000 : 0x00;
        accessBits |= ! blocksConditions.c1_2 ? 0x40000 : 0x00;
        accessBits |= ! sectorTrailerConditions.c1 ? 0x80000 : 0x00;

        accessBits |= ! blocksConditions.c2_0 ? 0x100000 : 0x00;
        accessBits |= ! blocksConditions.c2_1 ? 0x200000 : 0x00;
        accessBits |= ! blocksConditions.c2_2 ? 0x400000 : 0x00;
        accessBits |= ! sectorTrailerConditions.c2 ? 0x800000 : 0x00;

        accessBitsText.text = accessBits.toString(16).toUpperCase()
    }

    function showParseAccessBitsError()
    {
        errorText.text = qsTr("Error in control bit. Some inverted bits have invalid values.")
        errorMessage.visible = true
    }

    Component.onCompleted: decodeString(accessBitsText.text)
}
